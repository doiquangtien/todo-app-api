import React from "react";
import { Route, Routes } from "react-router-dom";
import List from "../container/List";
import Layout from "../container/Layout";
import Todos from "../container/Todos";
function Router() {
  return (
    <Layout>
      <Routes>
        <Route path="/">
          <Route index element={<List />} />
        </Route>
        <Route path="/list/:id">
          <Route index element={<Todos />} />
        </Route>
      </Routes>
    </Layout>
  );
}

export default Router;
