import { legacy_createStore as createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "../reducer/reducer";

// const rootReducer = combineReducers({
//   theDefaultReducer,
//   firstNamedReducer,
//   secondNamedReducer,
// });

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export default store;
