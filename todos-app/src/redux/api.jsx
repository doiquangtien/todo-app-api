import axios from "axios";
import {
  getTodo,
  deleteTodo,
  postTodo,
  updateTodo,
  getListAction,
  postListAction,
  deleteListAction,
  getListByIdAction,
} from "./action";

export const getListApi = async (dispatch) => {
  try {
    const res = await axios.get(`${process.env.REACT_APP_API}/list`);
    dispatch(getListAction(res.data));
  } catch (err) {
    console.log(err);
  }
};

export const getListById = async (dispatch, id) => {
  try {
    const res = await axios.get(`${process.env.REACT_APP_API}/list/${id}`);
    dispatch(getListByIdAction(res.data));
  } catch (err) {
    console.log(err);
  }
};

export const postListApi = async (dispatch, data) => {
  try {
    const res = await axios.post(`${process.env.REACT_APP_API}/list`, data);
    dispatch(postListAction(res.data));
  } catch (err) {
    console.log(err);
  }
};

export const deleteListApi = async (dispatch, id) => {
  try {
    dispatch(deleteListAction(id));
    await axios.delete(`${process.env.REACT_APP_API}/list/${id}`);
  } catch (err) {
    console.log(err);
  }
};

export const getTodoApi = async (dispatch, id) => {
  try {
    const res = await axios.get(
      `${process.env.REACT_APP_API}/list/${id}/todos`
    );
    dispatch(getTodo(res.data));
  } catch (err) {
    console.log(err);
  }
};

export const postTodoApi = async (dispatch, data, id, handleOpenToast) => {
  try {
    const res = await axios.post(
      `${process.env.REACT_APP_API}/list/${id}/todos
    `,
      data
    );
    dispatch(postTodo(res.data));
    handleOpenToast();
  } catch (err) {
    console.log(err);
  }
};

export const deleteTodoApi = async (dispatch, id_list, id, handleOpenToast) => {
  try {
    dispatch(deleteTodo(id));
    await axios.delete(
      `${process.env.REACT_APP_API}/list/${id_list}/todos/${id}`
    );
    handleOpenToast();
  } catch (err) {
    console.log(err);
  }
};

export const editTodoApi = async (
  dispatch,
  data,
  id_list,
  id,
  handleOpenToast
) => {
  try {
    await axios.put(
      `${process.env.REACT_APP_API}/list/${id_list}/todos/${id}`,
      data
    );
    dispatch(updateTodo(data, id));
    handleOpenToast();
  } catch (err) {
    console.log(err);
  }
};

export const checkStatusApi = async (dispatch, data, id_list, id) => {
  try {
    await axios.put(
      `${process.env.REACT_APP_API}/list/${id_list}/todos/${id}`,
      data
    );
    dispatch(updateTodo(data, id));
  } catch (err) {
    console.log(err);
  }
};
