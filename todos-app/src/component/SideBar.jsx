import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import { getListApi } from "../redux/api";
import HomeIcon from "@mui/icons-material/Home";

function SideBar() {
  const dispatch = useDispatch();
  const { id } = useParams();
  const navigate = useNavigate();
  const list = useSelector((state) => state.list);
  useEffect(() => {
    getListApi(dispatch);
  }, [dispatch]);
  return (
    <BoxStyle>
      <SideBarTop>
        <HomeIcon className="icon-home" onClick={() => navigate("/")} />
        <span>My List</span>
      </SideBarTop>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        value={id}
        name="radio-buttons-group"
      >
        {list?.map((item, i) => (
          <FormControlLabel
            key={i}
            value={item.id}
            control={<Radio />}
            label={item.namelist}
            onChange={() => {
              navigate(`/list/${item.id}`);
            }}
          />
        ))}
      </RadioGroup>
    </BoxStyle>
  );
}

export default SideBar;

const SideBarTop = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
  color: #1a76d2;
  .icon-home {
    margin-right: 20px;
    cursor: pointer;
    font-size: 30px;
  }
  span {
    font-size: 18px;
    font-weight: 600;
  }
`;

const BoxStyle = styled(Box)`
  border-radius: 16px;
  padding: 10px 20px;
  background: rgba(255, 255, 255, 1);
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid #91d5ff;
`;
