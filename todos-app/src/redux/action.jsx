export const getTodoInputForm = (data) => {
  return {
    type: "GET_INPUT_FORM",
    payload: data,
  };
};

export const getListAction = (text) => {
  return {
    type: "GET_LIST",
    payload: text,
  };
};

export const getListByIdAction = (data) => {
  return {
    type: "GET_LIST_BY_ID",
    payload: data,
  };
};

export const deleteListAction = (index) => {
  return {
    type: "DELETE_LIST",
    payload: index,
  };
};

export const postListAction = (data) => {
  return {
    type: "POST_LIST",
    payload: data,
  };
};

export const getTodo = (data) => {
  return {
    type: "GET_TODO",
    payload: data,
  };
};
export const postTodo = (data) => {
  return {
    type: "POST_TODO",
    payload: data,
  };
};
export const deleteTodo = (index) => {
  return {
    type: "DELETE_TODO",
    payload: index,
  };
};
export const updateTodo = (data, index) => {
  return {
    type: "UPDATE_TODO",
    payload: data,
    index: index,
  };
};

export const searchTextAction = (text) => {
  return {
    type: "SEARCH_TEXT",
    payload: text,
  };
};

export const searchStatusAction = (text) => {
  return {
    type: "SEARCH_STATUS",
    payload: text,
  };
};

export const resetToastMessageAction = {
  type: "RESET_TOAST",
  payload: {
    message: "",
    severity: "success",
  },
};
