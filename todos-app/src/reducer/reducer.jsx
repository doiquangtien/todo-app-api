const initValue = {
  filter: {
    searchText: "",
    searchStatus: "All",
  },
  list: [],
  listById: {},
  todoInputFrom: {
    name: "",
    todo: "",
    status: false,
    priority: "High",
  },
  todoList: [],
  toast: {
    message: "",
    severity: "success",
  },
};
const rootReducer = (state = initValue, action) => {
  switch (action.type) {
    case "GET_LIST":
      return {
        ...state,
        list: action.payload,
      };
    case "POST_LIST":
      return {
        ...state,
        toast: {
          message: "Add list successful !",
          severity: "success",
        },
        list: [...state.list, action.payload],
      };
    case "GET_LIST_BY_ID":
      return {
        ...state,

        listById: action.payload,
      };
    case "DELETE_LIST":
      const newDeleteList = [...state.list].filter(
        (item) => item.id !== action.payload
      );
      return {
        ...state,
        list: newDeleteList,
      };

    case "GET_INPUT_FORM":
      return {
        ...state,
        todoInputFrom: action.payload,
      };

    case "GET_TODO":
      return {
        ...state,
        todoList: action.payload,
      };
    case "POST_TODO":
      return {
        ...state,
        toast: {
          message: "Add task successful !",
          severity: "success",
        },
        todoList: [...state.todoList, action.payload],
      };
    case "UPDATE_TODO":
      const newUpdateList = [...state.todoList].map((item) => {
        if (item.id === action.index) {
          return { ...item, ...action.payload };
        }
        return item;
      });
      return {
        ...state,
        toast: {
          message: "Edit task successful !",
          severity: "success",
        },
        todoList: newUpdateList,
      };
    case "DELETE_TODO":
      const newDeleteListTodo = [...state.todoList].filter(
        (item) => item.id !== action.payload
      );
      return {
        ...state,
        toast: {
          message: "Delete task successful !",
          severity: "success",
        },
        todoList: newDeleteListTodo,
      };
    case "SEARCH_TEXT":
      return {
        ...state,
        filter: { ...state.filter, searchText: action.payload },
      };
    case "SEARCH_STATUS":
      return {
        ...state,
        filter: { ...state.filter, searchStatus: action.payload },
      };
    case "RESET_TOAST":
      return {
        ...state,
        toast: action.payload,
      };
    default:
      return state;
  }
};

export default rootReducer;
