import { Box, Button, Grid, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import AccordionItem from "../component/AccordionItem";
import { deleteListApi, getListApi, postListApi } from "../redux/api";
import ToastMessage from "../component/ToastMessage";
import DialogComfirm from "../component/DialogComfirm";

function List() {
  const dispatch = useDispatch();
  const list = useSelector((state) => state.list);
  const [valueInput, setValueInput] = useState("");
  const [openComfirm, setOpenComfirm] = React.useState(false);
  const [idList, setIdList] = useState("");
  const [openToast, setOpenToast] = React.useState(false);

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  useEffect(() => {
    getListApi(dispatch);
  }, [dispatch]);

  const handleAddList = () => {
    if (valueInput === "") return;
    postListApi(dispatch, {
      createdAt: new Date().toISOString(),
      namelist: valueInput,
    }).then(() => {
      handleOpenToast();
    });
    setValueInput("");
  };

  const handleDeleteList = (id) => {
    deleteListApi(dispatch, id);
  };

  return (
    <>
      <ToastMessage open={openToast} setOpen={setOpenToast} />
      <DialogComfirm
        title="Comfirm"
        context="Are you sure you want to delete?"
        open={openComfirm}
        setOpen={setOpenComfirm}
        handleDelete={() => {
          handleDeleteList(idList);
        }}
      />
      <BoxStyle>
        <Grid container spacing={2}>
          <Grid className="grid-top" item xs={12}>
            <TextField
              value={valueInput}
              onChange={(e) => {
                setValueInput(e.target.value);
              }}
              className="grid-top-input"
              id="standard-basic"
              label="Add your list"
              variant="standard"
            />
            <Button variant="contained" onClick={handleAddList}>
              Add List
            </Button>
          </Grid>
          <Grid item xs={12}>
            {list?.map((item, i) => (
              <AccordionItem
                key={i}
                data={item}
                handleOpenComfirm={() => {
                  setOpenComfirm(true);
                  setIdList(item.id);
                }}
              />
            ))}
          </Grid>
        </Grid>
      </BoxStyle>
    </>
  );
}

export default List;

const BoxStyle = styled(Box)`
  padding: 10px 20px;
  position: relative;
  top: 100px;
  width: 800px;
  margin: auto;
  background: rgba(255, 255, 255, 1);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid #91d5ff;
  .grid-top {
    display: flex;
    align-items: center;
    width: 100%;
    .grid-top-input {
      width: 40%;
      margin-right: 30px;
    }
  }
`;
